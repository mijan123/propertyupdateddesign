$(document).ready(function () {
    $('select.selector').select2();
})

// select2
// var $select2 = $('.js-select2');

// $select2.each(function () {

//     var $this = $(this);

//     if ($this.data('html')) {

//         function formatOption(data) {
//             return $('<span>' + data.text + '</span>');
//         };

//         $this.select2({
//             templateResult: formatOption,
//             templateSelection: formatOption
//         });

//     } else {
//         $this.select2();
//     }
// });
function goBack() {
    window.history.back();
}
function layoutChange(e) {
    const listingWrapper = document.querySelectorAll('.listing-wrapper')
    if (!e.classList.contains('active')) {
        document.querySelectorAll('.show-button').forEach((but, index) => {
            but.classList.remove('active')
            console.log(but)
        })
        e.classList.add('active')
        if (e.dataset.toggle == 'single') {
            listingWrapper.forEach((list) => {
                list.classList.remove('multiple')
                list.classList.add('single')
            })
        }
        if (e.dataset.toggle == 'multiple') {
            listingWrapper.forEach((list) => {
                list.classList.add('multiple')
                list.classList.remove('single')
            })
        }
    }
}

const menuImages = [
    {
        src: "./images/shopping-bag.png"
    },
    {
        src: "./images/building.png"
    },
    {
        src: "./images/briefcase.png"
    },
    {
        src: "./images/mortarboard.png"
    },
    {
        src: "./images/bar-chart.png"
    },
    {
        src: "./images/gamepad.png"
    }
]
const menuHolder = document.querySelector('.burger-menu')
const burgerButton = document.querySelector('.burger-toggler > button')
menuFuctions = () => {
    menuHolder.style.display = 'none'
    links = menuHolder.querySelectorAll('.menu-item>a');
    links.forEach((link, index) => {
        var menuImageHolder = document.createElement('span')
        menuImageHolder.classList.add('menu-image-holder')
        menuImageHolder.innerHTML = `<img src="${menuImages[index].src}">`
        link.prepend(menuImageHolder)
    })
}
menuHolder && menuFuctions()
function menuOpen() {
    burgerButton.classList.add('active')
    menuHolder.classList.remove('not-active')
    menuHolder.classList.add('active')
    menuHolder.style.display = 'block'
}
function menuClose() {
    burgerButton.classList.remove('active')
    menuHolder.classList.add('not-active')
    menuHolder.classList.remove('active')
    setTimeout(() => { menuHolder.style.display = 'none' }, 450)
}
menuToggle = () => {
    var menuOverlay = document.createElement('div')
    menuOverlay.classList.add('menu-overlay')
    const checkOverlay = menuHolder.querySelector('.menu-overlay')
    menuOverlay.setAttribute('onclick', 'menuClose()')
    !checkOverlay && menuHolder.append(menuOverlay)
    menuHolder.style.display == 'none' ? menuOpen() : menuClose()
}

$(function () {
    $("#slider-range").slider({
        range: true,
        min: 0,
        max: 3500,
        values: [0, 3500],
        slide: function (event, ui) {
            $("#amount").val("$" + ui.values[0] + " - $" + ui.values[1]);
        }
    });
    $("#amount").val("$" + $("#slider-range").slider("values", 0) +
        " - $" + $("#slider-range").slider("values", 1));
});


$(function () {
    $("#slider-size-range").slider({
        range: true,
        min: 0,
        max: 3000,
        values: [0, 3000],
        slide: function (event, ui) {
            $("#size").val(ui.values[0] + " sqft " + ui.values[1] + "sqft");
        }
    });
    $("#size").val($("#slider-size-range").slider("values", 0) +
        " sqft ~ " + $("#slider-size-range").slider("values", 1) + " sqft");
});

$('.pr-types-list ul li ').on('click', function (e) {
    e.preventDefault();
    $(this).siblings().removeClass('active');
    $(this).addClass('active');
});

$('.prop-room-list ul li ').on('click', function (e) {
    e.preventDefault();
    $(this).siblings().removeClass('active');
    $(this).addClass('active');
});

$('.prop-furn-list ul li ').on('click', function (e) {
    e.preventDefault();
    $(this).siblings().removeClass('active');
    $(this).addClass('active');
});


$('button#add-btn').on('click', function (e) {
    e.preventDefault();
    var $fltradv = $('#fltr-adv-clone').clone()
    $('.fltr-options-input-contents').append($fltradv);
})


